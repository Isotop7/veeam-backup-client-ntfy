# veeam-backup-client-ntfy

## How does it work?

This small scripts gets the status of the last backup and sends the information via [`Ntfy`](https://ntfy.sh/) to the recipient.

In order to function properly, you need to have elevated permissions, the `veeam` client and `curl`

Before using the script, please adjust the variables 

- `topicIdentifier`
- `priority`
- `icon`
- `ntfyHostname` (if ntfy is self-hosted)

to your needs.

## Why does it use an initial delay?

In order to wait for other post-backup tasks to settle, we just wait a few seconds to not return RUNNING states.

## What does it need?

- `curl`
- Veeam Backup Client