#!/bin/bash

# User variables
runDelay=30                                 # Initial delay before collecting statistics, see README.md
ntfyHostname="ntfy.sh"                      # Please adjust if you are using a self-hosted ntfy instance
topicIdentifier=""                          # Identifier of your private ntfy topic, can be created after registration
priority="3"                                # Priority of notification, see https://docs.ntfy.sh/publish/#message-priority
icon="loudspeaker"                          # Icon for notification, see https://docs.ntfy.sh/publish/#tags-emojis


# Check for root privileges
if [[ "$EUID" -ne 0 ]]
then
    echo "[ERROR] Script has to be run as root user in order to call veeam client"
    exit 1
fi

# Check for veeamconfig
if ! [[ -x "$(command -v veeamconfig)" ]]
then
    echo "[ERROR] Command 'veeamconfig' was not found. Please check if veeam client is correctly installed"
    exit 2
fi

# Check for curl
if ! [[ -x "$(command -v curl)" ]]
then
    echo "[ERROR] Command 'curl' was not found. Please check if curl is correctly installed"
    exit 3
fi

# Wait for other tasks to settle
sleep $runDelay

# When no job is running or running job finished, get variables from veeamconfig
lastBackup=$(veeamconfig session list | sed -n 'x;$p')
lastBackupID=$(echo $lastBackup | cut -d' ' -f3)
lastBackupState=$(echo $lastBackup | cut -d' ' -f4)
lastBackupStart=$(echo $lastBackup | cut -d' ' -f5,6)
lastBackupEnd=$(echo $lastBackup | cut -d' ' -f7,8)
lastBackupLog=$(veeamconfig session log --id $lastBackupID)

# Buils title
title="Backup with id '${lastBackupID}' ended with state ${lastBackupState^^}"

# Build message
message=$(cat << EOL
Status: ${lastBackupState^^}
Start:  ${lastBackupStart}
End:    ${lastBackupEnd}

Log:
------
${lastBackupLog}
EOL
)

# Send notification
curl \
    -H "X-Title: $title" \
    -H "X-Priority: $priority" \
    -H "X-Tags: $icon" \
    -d "$message" \
    $ntfyHostname/$topicIdentifier